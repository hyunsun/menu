json.array!(@language_maps) do |language_map|
  json.extract! language_map, :id, :code, :lan_text, :language_id
  json.url language_map_url(language_map, format: :json)
end

json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :id, :name, :address, :Rep, :phone_number, :open_time, :close_time, :extra_info
  json.url restaurant_url(restaurant, format: :json)
end

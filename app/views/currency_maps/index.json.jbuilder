json.array!(@currency_maps) do |currency_map|
  json.extract! currency_map, :id, :language_id, :language_code, :price
  json.url currency_map_url(currency_map, format: :json)
end

json.array!(@restaurant_photos) do |restaurant_photo|
  json.extract! restaurant_photo, :id, :restaurant_id
  json.url restaurant_photo_url(restaurant_photo, format: :json)
end

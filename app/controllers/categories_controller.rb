class CategoriesController < ApplicationController
  before_action :set_restaurant
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = @restaurant.categories.all
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
    
  end

  # GET /categories/new
  def new
    @category = @restaurant.categories.new
  end

  # GET /categories/1/edit
  def edit
    @category = @restaurant.categories.find(params[:id])
  end

  # POST /categories
  # POST /categories.json
  def create

    language_category = Language.create
    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:name].blank?
        language_category.language_maps.create(code: l, lan_text: params[l][:name])
      end
    end    

    @category = @restaurant.categories.new(name_id: language_category.id, restaurant_id: params[:restaurant_id])

    respond_to do |format|
      if @category.save
        format.html { redirect_to [@category.restaurant], notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    language_category = Language.find(@category.name_id)
    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:name].blank?
        language_map = LanguageMap.find_by language_id: language_category, code: l
        if !language_map.nil?
          language_map.update_column(:lan_text, params[l][:name])
        end
      end
    end
    respond_to do |format|
      if !language_category.nil?
        format.html { redirect_to [@category.restaurant, @categories], notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    language = Language.find(@category.name_id)
    language.destroy
    respond_to do |format|
      format.html { redirect_to [@category.restaurant], notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:name)
    end
end

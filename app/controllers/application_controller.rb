class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale, :set_currency

  def set_locale
  	if !params[:locale].blank?
  	  I18n.locale = session[:user_language] = params[:locale]
  	elsif !session[:user_language].blank?
  		I18n.locale = session[:user_language] || I18n.default_locale
    else
      I18n.locale = session[:user_language] = "ko"
    end	
  end

  def set_currency
    if !params[:currency].blank?
      session[:user_currency] = params[:currency]
    elsif session[:user_currency].blank?
      session[:user_currency] = "KRW"
    end
  end
end

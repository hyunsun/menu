class CurrencyMapsController < ApplicationController
  before_action :set_currency_map, only: [:show, :edit, :update, :destroy]

  # GET /currency_maps
  # GET /currency_maps.json
  def index
    @currency_maps = CurrencyMap.all
  end

  # GET /currency_maps/1
  # GET /currency_maps/1.json
  def show
  end

  # GET /currency_maps/new
  def new
    @currency_map = CurrencyMap.new
  end

  # GET /currency_maps/1/edit
  def edit
  end

  # POST /currency_maps
  # POST /currency_maps.json
  def create
    @currency_map = CurrencyMap.new(currency_map_params)

    respond_to do |format|
      if @currency_map.save
        format.html { redirect_to @currency_map, notice: 'Currency map was successfully created.' }
        format.json { render :show, status: :created, location: @currency_map }
      else
        format.html { render :new }
        format.json { render json: @currency_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /currency_maps/1
  # PATCH/PUT /currency_maps/1.json
  def update
    respond_to do |format|
      if @currency_map.update(currency_map_params)
        format.html { redirect_to @currency_map, notice: 'Currency map was successfully updated.' }
        format.json { render :show, status: :ok, location: @currency_map }
      else
        format.html { render :edit }
        format.json { render json: @currency_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /currency_maps/1
  # DELETE /currency_maps/1.json
  def destroy
    @currency_map.destroy
    respond_to do |format|
      format.html { redirect_to currency_maps_url, notice: 'Currency map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currency_map
      @currency_map = CurrencyMap.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def currency_map_params
      params.require(:currency_map).permit(:language_id, :language_code, :price)
    end
end

class LanguageMapsController < ApplicationController
  before_action :set_language_map, only: [:show, :edit, :update, :destroy]

  # GET /language_maps
  # GET /language_maps.json
  def index
    @language_maps = LanguageMap.all
  end

  # GET /language_maps/1
  # GET /language_maps/1.json
  def show
  end

  # GET /language_maps/new
  def new
    @language_map = LanguageMap.new
  end

  # GET /language_maps/1/edit
  def edit
  end

  # POST /language_maps
  # POST /language_maps.json
  def create
    @language_map = LanguageMap.new(language_map_params)

    respond_to do |format|
      if @language_map.save
        format.html { redirect_to @language_map, notice: 'Language map was successfully created.' }
        format.json { render :show, status: :created, location: @language_map }
      else
        format.html { render :new }
        format.json { render json: @language_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /language_maps/1
  # PATCH/PUT /language_maps/1.json
  def update
    respond_to do |format|
      if @language_map.update(language_map_params)
        format.html { redirect_to @language_map, notice: 'Language map was successfully updated.' }
        format.json { render :show, status: :ok, location: @language_map }
      else
        format.html { render :edit }
        format.json { render json: @language_map.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /language_maps/1
  # DELETE /language_maps/1.json
  def destroy
    @language_map.destroy
    respond_to do |format|
      format.html { redirect_to language_maps_url, notice: 'Language map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_language_map
      @language_map = LanguageMap.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def language_map_params
      params.require(:language_map).permit(:code, :lan_text, :language_id)
    end
end

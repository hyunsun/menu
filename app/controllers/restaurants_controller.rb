class RestaurantsController < ApplicationController
  before_action :authenticate_user!, only: [:new, :create, :update, :destroy]
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  # GET /restaurants
  # GET /restaurants.json
  def index
    @restaurants = Restaurant.all
    if current_user
      @cart = Cart.find_by(user_id: current_user.id)
    end
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @restaurant = Restaurant.find(params[:id])
    @menus = @restaurant.menus.all
    @categories = @restaurant.categories.all
    @restaurant_photos = @restaurant.restaurant_photos.all
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /restaurants/1/edit
  def edit
  end

  # POST /restaurants
  # POST /restaurants.json
  def create

    @restaurant = Restaurant.create(name: params[:restaurant][:name], address: params[:restaurant][:address],
     Rep: params[:restaurant][:Rep], phone_number: params[:restaurant][:phone_number], extra_info: params[:restaurant][:extra_info],
     open_time: params[:open_time], close_time: params[:close_time], user_id: current_user.id)

    if params[:images]
      params[:images].each { |image|
        @restaurant.restaurant_photos.create(restaurant_id: @restaurant.id, avatar: image)
      }
    end

    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    rest = RestaurantPhoto.find_by restaurant_id: @restaurant.id
    user = current_user.id
    if !rest.nil?
      rest.update_column(:avatar, params[:images])
    end

    respond_to do |format|
      if @restaurant.update(name: params[:restaurant][:name], address: params[:restaurant][:address],
     Rep: params[:restaurant][:Rep], phone_number: params[:restaurant][:phone_number], extra_info: params[:restaurant][:extra_info],
     open_time: params[:open_time], close_time: params[:close_time], user_id: user)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy

    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :address, :Rep, :phone_number, :extra_info)
    end
end

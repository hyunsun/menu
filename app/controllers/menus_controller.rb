class MenusController < ApplicationController
  before_action :set_restaurant
  before_action :set_menu, only: [:show, :edit, :update, :destroy]

  # GET /menus
  # GET /menus.json
  def index
    @menus = @restaurant.menus.all
    @categories = @restaurant.categories.all
    
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    @menu = Menu.find(params[:id])
    if @menu.category_id
      @category = Category.find(@menu.category_id)
    end
    @lang = Language.find(@menu.language_id)
    @desc = Language.find(@menu.desc_id)
    @curr = Currency.find(@menu.currency_id)

    # @cart = Cart.create(user_id: current_user, menu_id: @menu.id)

  end

  # GET /menus/new
  def new
    @menu = @restaurant.menus.new
    @categories = Category.where(restaurant_id: params[:restaurant_id])
    
    # @categories.each do |c|
    #   @language_map = LanguageMap.find_by language_id: c.name_id, code: "ko"
    #   @cate = @language_map.lan_text
    # end
    # @language_map = LanguageMap.find_by language_id: @categories, code: "ko"
    # @cate = @language_map.lan_text
  end

  # GET /menus/1/edit
  def edit
    @menu = Menu.find(params[:id])
    @categories = Category.where(restaurant_id: params[:restaurant_id])
  end

  # POST /menus
  # POST /menus.json
  def create

    # insert menu name

    language_name = Language.create

    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:name].blank?
        language_name.language_maps.create(code: l, lan_text: params[l][:name])
      end
    end

    # insert description

    language_desc = Language.create

    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:description].blank?
        language_desc.language_maps.create(code: l, lan_text: params[l][:description])
      end
    end

    # insert currency

    currency = Currency.create

    Currency::EXCHANGES.keys.each do |l|
      if !params[l].blank? && !params[l][:currency].blank?
        currency.exchange_maps.create(currency_code: l, price: params[l][:currency])
      end
    end

    #create menu

    @menu = @restaurant.menus.create(language: language_name, category_id: params[:category_id], desc_id: language_desc.id, currency: currency, restaurant_id: params[:restaurant_id])
   
    if params[:images]
      params[:images].each { |image|
        @menu.menu_photos.create(menu_id: @menu.id, avatar: image)
      }
    end

    respond_to do |format|
      if @menu.save
        format.html { redirect_to restaurant_menus_path, notice: 'Menu was successfully created.' }
        format.json { render :show, status: :created, location: @menu }
      else
        format.html { render :new }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    mphoto = MenuPhoto.find_by menu_id: @menu.id
    if !mphoto.nil?
      mphoto.update_column(:avatar, params[:images])
    end
    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:name].blank?
        language_map = LanguageMap.find_by language_id: @menu.language_id, code: l
        if !language_map.nil?
          language_map.update_column(:lan_text, params[l][:name])
        end
      end
    end

    Language::LANGUAGES.keys.each do |l|
      if !params[l].blank? && !params[l][:description].blank?
        language_map = LanguageMap.find_by language_id: @menu.desc_id, code: l
        if !language_map.nil?
          language_map.update_column(:lan_text, params[l][:description])
        end
      end
    end

    Currency::EXCHANGES.keys.each do |l|
      if !params[l].blank? && !params[l][:currency].blank?
        language_map = ExchangeMap.find_by currency_id: @menu.currency_id, currency_code: l
        if !language_map.nil?
          language_map.update_column(:price, params[l][:currency])
        end
      end
    end
    @menu = Menu.find(params[:id])
    @menu.update_column(:category_id, params[:category_id])

    respond_to do |format|
      if @menu.save
        format.html { redirect_to restaurant_menus_path, notice: 'Menu was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    # Language::LANGUAGES.keys.each do |l|
    #   languagemap = LanguageMap.find_by language_id: @menu.language_id, code: l
    #   languagemap.destroy
    # end
    # language = Language.find(@menu.language_id)
    # language.destroy
    # Language::LANGUAGES.keys.each do |l|
    #   languagemap = LanguageMap.find_by language_id: @menu.desc_id, code: l
    #   languagemap.destroy
    # end
    # language = Language.find(@menu.desc_id)
    # language.destroy
    
    @menu.destroy
    language = Language.find(@menu.language_id)
    language.destroy
    language = Language.find(@menu.desc_id)
    language.destroy
    currency = Currency.find(@menu.currency_id)
    currency.destroy
    respond_to do |format|
      format.html { redirect_to restaurant_menus_path }
      format.json { head :no_content }
    end
  end

  private
    def set_restaurant
      @restaurant = Restaurant.find(params[:restaurant_id])
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:name, :price, :description, :category_id, :language_id)
    end
end

class ExchangeMapsController < ApplicationController
	before_action :set_exchange_map, only: [:show, :edit, :update, :destroy]

  # GET /exchange_maps
  # GET /exchange_maps.json
  def index
    @exchange_maps = ExchangeMap.all
  end

  # GET /exchange_maps/1
  # GET /exchange_maps/1.json
  def show
  end

  # GET /exchange_maps/new
  def new
    @exchange_map = ExchangeMap.new
  end

  # GET /exchange_maps/1/edit
  def edit
  end

  # POST /exchange_maps
  # POST /exchange_maps.json
  def create
    @exchange_maps = ExchangeMap.new(exchange_maps_params)

    respond_to do |format|
      if @exchange_maps.save
        format.html { redirect_to @exchange_maps, notice: 'Exchange map was successfully created.' }
        format.json { render :show, status: :created, location: @exchange_maps }
      else
        format.html { render :new }
        format.json { render json: @exchange_maps.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /language_maps/1
  # PATCH/PUT /language_maps/1.json
  def update
    respond_to do |format|
      if @exchange_maps.update(exchange_maps_params)
        format.html { redirect_to @exchange_maps, notice: 'Exchange map was successfully updated.' }
        format.json { render :show, status: :ok, location: @exchange_maps }
      else
        format.html { render :edit }
        format.json { render json: @exchange_maps.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /language_maps/1
  # DELETE /language_maps/1.json
  def destroy
    @exchange_maps.destroy
    respond_to do |format|
      format.html { redirect_to exchange_maps_url, notice: 'Exchange map was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exchange_maps
      @exchange_maps = ExchangeMap.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exchange_maps_params
      params.require(:exchange_maps).permit(:currency_code, :price, :currency_id)
    end
end

class MenuPhotosController < ApplicationController
  before_action :set_menu_photo, only: [:show, :edit, :update, :destroy]

  # GET /menu_photos
  # GET /menu_photos.json
  def index
    @menu_photos = MenuPhoto.all
  end

  # GET /menu_photos/1
  # GET /menu_photos/1.json
  def show
  end

  # GET /menu_photos/new
  def new
    @menu_photo = MenuPhoto.new
  end™

  # GET /menu_photos/1/edit
  def edit
  end

  # POST /menu_photos
  # POST /menu_photos.json
  def create
    @menu_photo = MenuPhoto.new(menu_photo_params)

    respond_to do |format|
      if @menu_photo.save
        format.html { redirect_to @menu_photo, notice: 'Menu photo was successfully created.' }
        format.json { render :show, status: :created, location: @menu_photo }
      else
        format.html { render :new }
        format.json { render json: @menu_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menu_photos/1
  # PATCH/PUT /menu_photos/1.json
  def update
    respond_to do |format|
      if @menu_photo.update(menu_photo_params)
        format.html { redirect_to @menu_photo, notice: 'Menu photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu_photo }
      else
        format.html { render :edit }
        format.json { render json: @menu_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_photos/1
  # DELETE /menu_photos/1.json
  def destroy
    @menu_photo.destroy
    respond_to do |format|
      format.html { redirect_to menu_photos_url, notice: 'Menu photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_photo
      @menu_photo = MenuPhoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_photo_params
      params.require(:menu_photo).permit(:menu_id)
    end
end

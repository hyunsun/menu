class Restaurant < ActiveRecord::Base
	has_many :menus
	has_many :categories, dependent: :destroy
	has_many :restaurant_photos, dependent: :destroy

	def get_photo
		rphoto = RestaurantPhoto.find_by restaurant_id: self.id
		if !rphoto.nil?
			rphoto.avatar.url(:medium)
		else
			"http://placehold.it/350x150?text=no+image"
		end
	end
end

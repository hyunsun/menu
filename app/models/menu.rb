class Menu < ActiveRecord::Base
  belongs_to :category
  belongs_to :language
  belongs_to :currency
  belongs_to :restaurant
  has_many :menu_photos, dependent: :destroy
  has_many :carts

  def translation_name(locale)
  	language_map = self.language.language_maps.find_by_code(locale)
  	
  	if !language_map.nil?
  		language_map.lan_text
  	else
  		""
  	end
  end

  def translation_desc(locale)
    language_map = LanguageMap.find_by language_id: self.desc_id, code: locale
    
    if !language_map.nil?
      language_map.lan_text
    else
      ""
    end
  end

  def change_price(locale)
    exchange_map = ExchangeMap.find_by currency_id: self.currency_id, currency_code: locale
    if !exchange_map.nil?
      exchange_map.price
    else
      ""
    end
  end

  def translation_category(locale)
    if !self.category_id.nil?
      category = Category.find(self.category_id).name_id
      language_map = LanguageMap.find_by language_id: category, code: locale
    else
      ""
    end
    if !language_map.nil?
      language_map.lan_text
    else
      ""
    end
  end

  def get_mphoto
    mphoto = MenuPhoto.find_by menu_id: self.id
    if !mphoto.nil?
      mphoto.avatar.url(:medium)
    else
      "http://placehold.it/350x150?text=no+image"
    end
  end

end

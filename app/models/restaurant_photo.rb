class RestaurantPhoto < ActiveRecord::Base
  belongs_to :restaurant

  has_attached_file :avatar,
   styles: { medium: "600x300>", thumb: "100x100>" },
   default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def get_photo
		rphoto = RestaurantPhoto.find_by restaurant_id: self.id
		if !rphoto.nil?
			rphoto.avatar.url(:medium)
		else
			"http://placehold.it/350x150?text=no+image"
		end
	end
end

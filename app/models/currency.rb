class Currency < ActiveRecord::Base
	has_many :menus
	has_many :exchange_maps, dependent: :destroy

	DEFAULT_EXCHANGES = "KRW"

	EXCHANGES = {
		"KRW" => "₩",
		"USD" => "$",
		"JPY" => "¥",
		"CNY" => "Y"
	}
end

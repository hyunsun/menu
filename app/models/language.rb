class Language < ActiveRecord::Base
	has_many :language_maps, dependent: :destroy
	has_many :menus

	DEFAULT_LANGUAGE = "ko"
	
	LANGUAGES = {
		"ko" => "한국어",
		"en" => "English",
		"jp" => "日本語",
		"cn" => "中文"
	}

end

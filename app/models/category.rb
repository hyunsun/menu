class Category < ActiveRecord::Base
	has_many :menus, dependent: :destroy
	belongs_to :restaurant

	def translation_category(locale)
		language_map = LanguageMap.find_by language_id: self.name_id, code: locale
		if !language_map.nil?
  		language_map.lan_text
  	else
  		""
  	end
	end
end

class DeleteCurrencyIdColumnToMenus < ActiveRecord::Migration
  def change
  	remove_column :menus, :currency_id
  end
end

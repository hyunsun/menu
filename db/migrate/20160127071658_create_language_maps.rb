class CreateLanguageMaps < ActiveRecord::Migration
  def change
    create_table :language_maps do |t|
      t.string :code
      t.text :lan_text
      t.references :language, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

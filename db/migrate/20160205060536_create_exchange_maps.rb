class CreateExchangeMaps < ActiveRecord::Migration
  def change
    create_table :exchange_maps do |t|
    	t.string :currency_code
    	t.float :price
    	t.references :currency, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class DeleteDescPriceFromMenus < ActiveRecord::Migration
  def change
  	remove_column :menus, :description
  	remove_column :menus, :price
  end
end

class AddAttachmentAvatarToRestaurantPhotos < ActiveRecord::Migration
  def self.up
    change_table :restaurant_photos do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :restaurant_photos, :avatar
  end
end

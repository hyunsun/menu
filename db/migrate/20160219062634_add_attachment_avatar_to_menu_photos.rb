class AddAttachmentAvatarToMenuPhotos < ActiveRecord::Migration
  def self.up
    change_table :menu_photos do |t|
      t.attachment :avatar
    end
  end

  def self.down
    remove_attachment :menu_photos, :avatar
  end
end

class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :address
      t.string :Rep
      t.string :phone_number
      t.string :open_time
      t.string :close_time
      t.string :extra_info

      t.timestamps null: false
    end
  end
end

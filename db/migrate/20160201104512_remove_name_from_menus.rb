class RemoveNameFromMenus < ActiveRecord::Migration
  def change
    remove_column :menus, :name, :string
  end
end

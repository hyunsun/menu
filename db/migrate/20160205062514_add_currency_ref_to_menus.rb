class AddCurrencyRefToMenus < ActiveRecord::Migration
  def change
  	add_reference :menus, :currency, index: true
  end
end
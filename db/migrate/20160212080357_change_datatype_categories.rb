class ChangeDatatypeCategories < ActiveRecord::Migration
  def change
  	remove_column :categories, :name, :string
  	add_reference :categories, :name, references: :languages, index: true
  end
end

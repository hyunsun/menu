class CreateRestaurantPhotos < ActiveRecord::Migration
  def change
    create_table :restaurant_photos do |t|
      t.references :restaurant, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end

class AddReferenceToUserMenu < ActiveRecord::Migration
  def change
  	add_reference :carts, :user, index: true
  	add_reference :carts, :menu, index: true
  end
end

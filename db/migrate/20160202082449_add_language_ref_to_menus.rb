class AddLanguageRefToMenus < ActiveRecord::Migration
  def change
    add_reference :menus, :desc, references: :languages, index: true
    add_reference :menus, :currency, references: :languages, index: true
  end
end

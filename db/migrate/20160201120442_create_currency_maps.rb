class CreateCurrencyMaps < ActiveRecord::Migration
  def change
    create_table :currency_maps do |t|
      t.references :language, index: true, foreign_key: true
      t.string :language_code
      t.string :price

      t.timestamps null: false
    end
  end
end

require 'test_helper'

class RestaurantPhotosControllerTest < ActionController::TestCase
  setup do
    @restaurant_photo = restaurant_photos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurant_photos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant_photo" do
    assert_difference('RestaurantPhoto.count') do
      post :create, restaurant_photo: { restaurant_id: @restaurant_photo.restaurant_id }
    end

    assert_redirected_to restaurant_photo_path(assigns(:restaurant_photo))
  end

  test "should show restaurant_photo" do
    get :show, id: @restaurant_photo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restaurant_photo
    assert_response :success
  end

  test "should update restaurant_photo" do
    patch :update, id: @restaurant_photo, restaurant_photo: { restaurant_id: @restaurant_photo.restaurant_id }
    assert_redirected_to restaurant_photo_path(assigns(:restaurant_photo))
  end

  test "should destroy restaurant_photo" do
    assert_difference('RestaurantPhoto.count', -1) do
      delete :destroy, id: @restaurant_photo
    end

    assert_redirected_to restaurant_photos_path
  end
end

require 'test_helper'

class CurrencyMapsControllerTest < ActionController::TestCase
  setup do
    @currency_map = currency_maps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:currency_maps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create currency_map" do
    assert_difference('CurrencyMap.count') do
      post :create, currency_map: { language_code: @currency_map.language_code, language_id: @currency_map.language_id, price: @currency_map.price }
    end

    assert_redirected_to currency_map_path(assigns(:currency_map))
  end

  test "should show currency_map" do
    get :show, id: @currency_map
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @currency_map
    assert_response :success
  end

  test "should update currency_map" do
    patch :update, id: @currency_map, currency_map: { language_code: @currency_map.language_code, language_id: @currency_map.language_id, price: @currency_map.price }
    assert_redirected_to currency_map_path(assigns(:currency_map))
  end

  test "should destroy currency_map" do
    assert_difference('CurrencyMap.count', -1) do
      delete :destroy, id: @currency_map
    end

    assert_redirected_to currency_maps_path
  end
end

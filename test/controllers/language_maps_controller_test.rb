require 'test_helper'

class LanguageMapsControllerTest < ActionController::TestCase
  setup do
    @language_map = language_maps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:language_maps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create language_map" do
    assert_difference('LanguageMap.count') do
      post :create, language_map: { code: @language_map.code, lan_text: @language_map.lan_text, language_id: @language_map.language_id }
    end

    assert_redirected_to language_map_path(assigns(:language_map))
  end

  test "should show language_map" do
    get :show, id: @language_map
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @language_map
    assert_response :success
  end

  test "should update language_map" do
    patch :update, id: @language_map, language_map: { code: @language_map.code, lan_text: @language_map.lan_text, language_id: @language_map.language_id }
    assert_redirected_to language_map_path(assigns(:language_map))
  end

  test "should destroy language_map" do
    assert_difference('LanguageMap.count', -1) do
      delete :destroy, id: @language_map
    end

    assert_redirected_to language_maps_path
  end
end
